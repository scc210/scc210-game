import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.function.*;

import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;
import org.jsfml.graphics.*;

class menu {

	private static int healthpoint = 100;
	private static int gamepoints = 0;

	private static int screenWidth  = 1280;
	private static int screenHeight = 720;
	private static String gameversion = "1.0 RAW";
	
	private static int fontSize     = 25;
	private static String MenuFont  = "fonts/menu.ttf";
	private static String MenuBG = "img/menu.png";
	private static String MenuBG2 = "img/menuBGlayer2.png";
	private static String GameBG = "img/gamebg.png";
	private static String GameMap = "img/gamemap.png";
	private static String hudBG = "img/hudbg.png";
	private static String UserChar = "img/character.png";
	private static String EnemyChar = "img/badcharacter.png";
	private static String Title   = "UniAttack " + gameversion;

	private ArrayList<Entities> charent = new ArrayList<Entities>( );
	private ArrayList<staticEntities> charmisc = new ArrayList<staticEntities>( );

	private abstract class staticEntities {
		Drawable miscent;
		IntConsumer rotate;
		BiConsumer<Float, Float> setPosition;

		int x  = 0;	// Current X-coordinate
		int y  = 0;	// Current Y-coordinate
		int dx = 0;	// Change in X-coordinate per cycle
		int dy = 0;	// Change in Y-coordinate per cycle

		boolean checkbounds (int x, int y) {
			return false;
		}

		//
		// Work out where object should be for next frame
		//
		void genmove(int minx, int miny, int maxx, int maxy) {
			//
			// Add deltas to x and y position
			//
			x = x;
			y = y;

			//
			// Check we've not hit screen bounds
			//
			if (x <= minx || x >= maxx) { dx *= -1; x += dx; }
			if (y <= miny || y >= maxy) { dy *= -1; y += dy; }

			//
			// Check we've not collided with any other Entities
			//
			for (staticEntities a : charmisc) {
				if (a.miscent != miscent && a.checkbounds(x, y)) {
					dx *= -1; x += dx;
					dy *= -1; y += dy;
				}
			}
			}
			void generatepos( ) {
				setPosition.accept((float)x, (float)y);
			}
	
			void draw(RenderWindow w) {
				w.draw(miscent);
			}
}

	private abstract class Entities {
		Drawable ent;
		IntConsumer rotate;
		BiConsumer<Float, Float> setPosition;

		int x  = 0;	// Current X-coordinate
		int y  = 0;	// Current Y-coordinate
		int dx = 5;	// Change in X-coordinate per cycle
		int dy = 0;	// Change in Y-coordinate per cycle

		boolean checkbounds (int x, int y) {
			return false;
		}

		//
		// Work out where object should be for next frame
		//
		void genmove(int minx, int miny, int maxx, int maxy) {
			//
			// Add deltas to x and y position
			//
			x += dx;
			y += dy;

			//
			// Check we've not hit screen bounds
			//
			if (x <= minx || x >= maxx) { dx *= -1; x += dx; }
			if (y <= miny || y >= maxy) { dy *= -1; y += dy; }

			//
			// Check we've not collided with any other Entities
			//
			for (Entities a : charent) {
				if (a.ent != ent && a.checkbounds(x, y)) {
					dx *= -1; x += dx;
					dy *= -1; y += dy;
				}
			}
		}

		void generatepos( ) {
			setPosition.accept((float)x, (float)y);
		}

		void draw(RenderWindow w) {
			w.draw(ent);
		}
}
private class GameText extends staticEntities {
		private Text text;

		public GameText(int x, int y, int r, String msgtxt, Color c) {
			//
			// Load the font
			//
			Font sansRegular = new Font( );
			try {
				sansRegular.loadFromFile(
						Paths.get(MenuFont));
			} catch (IOException ex) {
				ex.printStackTrace( );
			}

			text = new Text (msgtxt, sansRegular, fontSize);
			text.setColor(c);
			text.setStyle(Text.BOLD);

			FloatRect textBounds = text.getLocalBounds( );
			// Find middle and set as origin/ reference point
			text.setOrigin(textBounds.width / 1,
					textBounds.height / 1);

			this.x = x;
			this.y = y;
			miscent = text;
			setPosition = text::setPosition;
		}
	}		 

	private class imgLayer extends staticEntities {
		private Sprite img;

		public imgLayer(int x, int y, int r, String textureFile) {
			//
			// Load image/ texture
			//
			Texture imgTexture = new Texture( );
			try {
				imgTexture.loadFromFile(Paths.get(textureFile));
			} catch (IOException ex) {
				ex.printStackTrace( );
			}
			imgTexture.setSmooth(true);

			img = new Sprite(imgTexture);
			img.setOrigin(Vector2f.div(
					new Vector2f(imgTexture.getSize()), 2));

			this.x = x;
			this.y = y;
			miscent = img;
			setPosition = img::setPosition;
		}
	}

		private class CharSprite extends Entities {
		private Sprite img;

		public CharSprite(int x, int y, int r, String textureFile) {
			//
			// Load image/ texture
			//
			Texture imgTexture = new Texture( );
			try {
				imgTexture.loadFromFile(Paths.get(textureFile));
			} catch (IOException ex) {
				ex.printStackTrace( );
			}
			imgTexture.setSmooth(true);
			//SetOrigin will give you information of the middle of the character
			img = new Sprite(imgTexture);
			img.setOrigin(Vector2f.div(
					new Vector2f(imgTexture.getSize()), 2));

			this.x = x;
			this.y = y;
			ent = img;
			setPosition = img::setPosition;
		}
	}

		public void mainmenu ( ) {

			//
			// Build Game Window
			//
			RenderWindow gamewindow = new RenderWindow( );
			gamewindow.create(new VideoMode(screenWidth, screenHeight),
					Title,
					WindowStyle.TITLEBAR | WindowStyle.CLOSE);
	
			gamewindow.setFramerateLimit(60); // Default set to 60fps
		


			//CharacterEnt Layers
			charmisc.add(new imgLayer(screenWidth / 2, screenHeight / 2,
					10, MenuBG));
			charent.add(new CharSprite(screenWidth / 2, 650,
					10, UserChar));
			charmisc.add(new imgLayer(screenWidth / 2, screenHeight / 2,
					10, MenuBG2));
			
			//
			// Game Refresh
			//
			while (gamewindow.isOpen( )) {
				// Clear the screen
				gamewindow.clear(Color.WHITE);
	

			

				// Move all the ent around
			for (staticEntities staticEntities : charmisc) {
				staticEntities.genmove(0, 0, screenWidth, screenHeight);
				staticEntities.generatepos( );
				staticEntities.draw(gamewindow);
				}
			for (Entities Entities : charent) {
				Entities.genmove(0, 0, screenWidth, screenHeight);
				Entities.generatepos( );
				Entities.draw(gamewindow);
				}

				// Update the display with any changes
				gamewindow.display( );
	
				// Handle any events
				for (Event event : gamewindow.pollEvents( )) {
					if (event.type == Event.Type.CLOSED) {
						// the user pressed the close button
						gamewindow.close( );
				}
			}
		}
	}
	
		//
		// Render the object at its new position
		//
		public static void main (String args[ ]) {

			//Coming Soon... (Main Menu Screen)
			//MainMenu x = new MainMenu();
			//x.menuscreen();
			menu t = new menu( );
			t.mainmenu( );
		
		}
	
}