import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.function.*;

import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;
import org.jsfml.graphics.*;

class character {

	private abstract class Entities {
		Drawable obj;
		IntConsumer rotate;
		BiConsumer<Float, Float> setPosition;

		int x  = 0;	// Current X-coordinate
		int y  = 0;	// Current Y-coordinate

		int r  = 0;	// Change in rotation per cycle
		int dx = 0;	// Change in X-coordinate per cycle
		int dy = 0;	// Change in Y-coordinate per cycle

		//
		// Is point x, y within area occupied by this object?
		//
		// This should really be done with bounding boxes not points
		//
		boolean within (int x, int y) {
			// Should check object bounds here
			// -- we'd normally assume a simple rectangle
			//    ...and override as necessary
			return false;
		}

		//
		// Work out where object should be for next frame
		//
		void calcMove(int minx, int miny, int maxx, int maxy) {
			//
			// Add deltas to x and y position
			//
			x += dx;
			y += dy;

			//
			// Check we've not hit screen bounds
			//
			if (x <= minx || x >= maxx) { dx *= -1; x += dx; }
			if (y <= miny || y >= maxy) { dy *= -1; y += dy; }

			//
			// Check we've not collided with any other Entities
			//
			for (Entities a : ent) {
				if (a.obj != obj && a.within(x, y)) {
					dx *= -1; x += dx;
					dy *= -1; y += dy;
				}
			}
		}

		//
		// Reposition the object
		//
		void performMove( ) {
			rotate.accept(r);
			setPosition.accept((float)x, (float)y);
		}

		//
		// Render the object at its new position
		//
		void draw(RenderWindow w) {
			w.draw(obj);
		}
	}

	private class Char extends Entities {
		private Sprite img;

		public Char(int x, int y, int r, String textureFile) {
			//
			// Load image/ texture
			//
			Texture imgTexture = new Texture( );
			try {
				imgTexture.loadFromFile(Paths.get(textureFile));
			} catch (IOException ex) {
				ex.printStackTrace( );
			}
			imgTexture.setSmooth(true);

			img = new Sprite(imgTexture);
			img.setOrigin(Vector2f.div(
					new Vector2f(imgTexture.getSize()), 2));

			this.x = x;
			this.y = y;
			//this.r = r;

			//
			// Store references to object and key methods
			//
			obj = img;
			rotate = img::rotate;
			setPosition = img::setPosition;
		}
	}

}	
