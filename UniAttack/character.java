import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.function.*;

import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;
import org.jsfml.graphics.*;

class Engine {
	private static int screenWidth  = 1280;
	private static int screenHeight = 720;
	private static String gameversion = "1.0 beta";
	
	private static int fontSize     = 28;
	private static String MenuFont  = "fonts/menu.ttf";
	private static String ImageFile = "img/logo.png";
	private static String BgFile = "img/bg.png";
	
	private static String Title   = "UniAttack " + gameversion;
	private static String TitleMsg = "Welcome to University Attack!";


	private ArrayList<Entities> ent = new ArrayList<Entities>( );

	private abstract class Entities {
		Drawable obj;
		IntConsumer rotate;
		BiConsumer<Float, Float> setPosition;

		int x  = 0;	// Current X-coordinate
		int y  = 0;	// Current Y-coordinate

		int r  = 0;	// Change in rotation per cycle
		int dx = 0;	// Change in X-coordinate per cycle
		int dy = 0;	// Change in Y-coordinate per cycle

		//
		// Is point x, y within area occupied by this object?
		//
		// This should really be done with bounding boxes not points
		//
		boolean within (int x, int y) {
			// Should check object bounds here
			// -- we'd normally assume a simple rectangle
			//    ...and override as necessary
			return false;
		}

		//
		// Work out where object should be for next frame
		//
		void calcMove(int minx, int miny, int maxx, int maxy) {
			//
			// Add deltas to x and y position
			//
			x += dx;
			y += dy;

			//
			// Check we've not hit screen bounds
			//
			if (x <= minx || x >= maxx) { dx *= -1; x += dx; }
			if (y <= miny || y >= maxy) { dy *= -1; y += dy; }

			//
			// Check we've not collided with any other Entities
			//
			for (Entities a : ent) {
				if (a.obj != obj && a.within(x, y)) {
					dx *= -1; x += dx;
					dy *= -1; y += dy;
				}
			}
		}

		//
		// Reposition the object
		//
		void performMove( ) {
			rotate.accept(r);
			setPosition.accept((float)x, (float)y);
		}

		//
		// Render the object at its new position
		//
		void draw(RenderWindow w) {
			w.draw(obj);
		}
	}

	private class GameText extends Entities {
		private Text text;

		public GameText(int x, int y, int r, String msgtxt, Color c) {
			//
			// Load the font
			//
			Font sansRegular = new Font( );
			try {
				sansRegular.loadFromFile(
						Paths.get(MenuFont));
			} catch (IOException ex) {
				ex.printStackTrace( );
			}

			text = new Text (msgtxt, sansRegular, fontSize);
			text.setColor(c);
			text.setStyle(Text.BOLD);

			FloatRect textBounds = text.getLocalBounds( );
			// Find middle and set as origin/ reference point
			text.setOrigin(textBounds.width / 2,
					textBounds.height / 2);

			this.x = x;
			this.y = y;
			//this.r = r;

			//
			// Store references to object and key methods
			//
			obj = text;
			rotate = text::rotate;
			setPosition = text::setPosition;
		}
	}

	private class Image extends Entities {
		private Sprite img;

		public Image(int x, int y, int r, String textureFile) {
			//
			// Load image/ texture
			//
			Texture imgTexture = new Texture( );
			try {
				imgTexture.loadFromFile(Paths.get(textureFile));
			} catch (IOException ex) {
				ex.printStackTrace( );
			}
			imgTexture.setSmooth(true);

			img = new Sprite(imgTexture);
			img.setOrigin(Vector2f.div(
					new Vector2f(imgTexture.getSize()), 2));

			//this.r = r;

			//
			// Store references to object and key methods
			//
			obj = img;
			rotate = img::rotate;
			setPosition = img::setPosition;

						
			//Assumed Control for Main Character //WASD + UP/DOWN/LEFT/RIGHT

			if (isKeyPressed(Keyboard.Key W) || (isKeyPressed(Keyboard.Key UP) = true)
			{
				setPosition = img::setPosition;
				this.x = x;
				this.y = y + 5;
			}
			if (isKeyPressed(Keyboard.Key A) || (isKeyPressed(Keyboard.Key LEFT) = true)
			{
				setPosition = img::setPosition;
				this.x = x - 5;
				this.y = y;
			}
			if (isKeyPressed(Keyboard.Key S) || (isKeyPressed(Keyboard.Key DOWN) = true)
			{
				setPosition = img::setPosition;
				this.x = x;
				this.y = y - 5;
			}
			if (isKeyPressed(Keyboard.Key D) || (isKeyPressed(Keyboard.Key DOWN) = true)
			{
				setPosition = img::setPosition;
				this.x = x + 5;
				this.y = y;
			}

		}
	}

	public void run ( ) {


		//
		// Create a window
		//
		RenderWindow window = new RenderWindow( );
		window.create(new VideoMode(screenWidth, screenHeight),
				Title,
				WindowStyle.TITLEBAR | WindowStyle.CLOSE);

		window.setFramerateLimit(60); // Default set to 60fps

		//
		// Create some Entities
		//
		ent.add(new Image(screenWidth / 2, screenHeight / 2,
					10, BgFile));
		ent.add(new Image(screenWidth / 2, screenHeight / 3 - 25,
					10, ImageFile));
		ent.add(new GameText(screenWidth / 2, screenHeight / 2,
					10, TitleMsg, Color.WHITE));
		//ent.add(new Bubble(500, 500, 20, Color.MAGENTA, 128));
		//ent.add(new Bubble(600, 600, 20, Color.YELLOW,  128));
		//ent.add(new Bubble(500, 600, 20, Color.BLUE,    128));
		//ent.add(new Bubble(600, 500, 20, Color.BLACK,   128));

		//
		// Main loop
		//
		while (window.isOpen( )) {
			// Clear the screen
			window.clear(Color.BLACK);

			// Move all the ent around
			for (Entities Entities : ent) {
				Entities.calcMove(0, 0, screenWidth, screenHeight);
				Entities.performMove( );
				Entities.draw(window);
			}

			// Update the display with any changes
			window.display( );

			// Handle any events
			for (Event event : window.pollEvents( )) {
				if (event.type == Event.Type.CLOSED) {
					// the user pressed the close button
					window.close( );
				}
			}
		}
	}

	public static void main (String args[ ]) {
		Engine t = new Engine( );
		t.run( );
	}
}	
