# Game-Project
This is the development tracking and file staging area for the game project, for SCC210 coursework

1. Jeremy
2. Aaron
3. Abhay
4. Aman
5. Alex

The game design report will be done as follow. (Current Progress)


-----


### D1: Design Report
This report documents your work during term 1 towards the initial ideas for your game and the design of the game your group has chosen to implement. The maximum number of pages is 20 and anything after page 20 will not be marked. The design report should include:

**Week 3 to week 5**
* **Various project/ game ideas from each group member**
* **Task list throughout the term (for weeks 1, 3, 5, and 7)**
* **Desired game features and gameplay**

**Week 5 to week 7**
* Software requirements
* Game design principles that influenced the design of your game
* Set of game rules for your game

**Week 7 to week 9**
* Project plan for implementation of game in term 2, including milestones and deliverables for weeks 9-19
* Acceptance tests
* Activity network with critical path, and Gantt chart 


-----

### D2: Final Presentation
To be written in the future.